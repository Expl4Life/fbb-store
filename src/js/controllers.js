bookApp.controller('BookListCtrl', function($scope, $http) {
    $http.get('https://netology-fbb-store-api.herokuapp.com/book/').success(function(data) {
        $scope.books = data;
        // $scope.miniArrQuantity = (Math.ceil(books.length/4));
    });
    $http.get('https://netology-fbb-store-api.herokuapp.com/order/delivery').success(function(data) {
        $scope.delivery = data;
    });
    $http.get('https://netology-fbb-store-api.herokuapp.com/order/payment').success(function(data) {
        $scope.payment = data;
    });
    $scope.showBookDetails = function(element){
        console.log("details");
    	element.preventDefault();
    	// console.log(element.currentTarget);
    	$scope.selectedBook = element.currentTarget.getAttribute("ng-href");
    	$http.get($scope.selectedBook).success(function(data) {
    	    $scope.singleBook = data;
    	    singleBook = data;
    	    var container = document.getElementById('main-container');
    	    var bookDetails = document.getElementById('bookDetails');
            var mainOrder = document.getElementById('main-order');
            var mainBook = document.querySelector('.main-book');
            console.log(mainBook);
            container.style.display = 'none';
            mainOrder.style.display = 'none';
            mainBook.style.display = 'block';
    	    bookDetails.style.display = 'block';
    	});
    };
    $scope.showAboutPage = function(element) {
    	if ($('.avatars').length != 0) {	
            console.log("e1");
    		element.preventDefault();	
    	}
        else {
    		$("header").append("<section id='about-container'></section>");
    		$('#about-container').load('about.html');
    		$('#about-container').show();
    		$('#main-container').hide();
    		$('#bookDetails').hide();
    		console.log("e2");
    	}
    };
    $scope.showMainPage = function(element) {
    		$('#main-container').show();
    		$('#about-container').remove();
    		$('#bookDetails').hide();
	};
    $scope.showOrderPage = function(element) {
        var mainOrder = document.getElementById('main-order');
        var orderList = document.getElementById('order-list');
        var errorBlock = document.getElementById('error-block');
        var mainBook = document.querySelector('.main-book');
        mainOrder.style.display = 'block';
        orderList.style.display = 'block';
        mainBook.style.display = 'none';
        errorBlock.style.display = 'none';
    };
    $scope.sendOrder = function(element){
        var book = document.getElementById('book-id').innerText;
        var name = document.getElementById('name').value;
        var phone = document.getElementById('phone').value;
        var email = document.getElementById('email').value;
        var comment = document.getElementById('comment').value;
        var deliveryType = $('#delivery-01:checked');
        if (deliveryType.length > 0) {
            var deliveryId = document.getElementById('delivery-01').value;
            var deliveryAddress = "";
        } else {
            var deliveryId = document.getElementById('delivery-02').value;
            var deliveryAddress = document.getElementById('deliveryAdress').value;
        }
        var paymentType = $('#payment-01:checked');
        console.log(deliveryType);
        if (paymentType.length > 0) {
            var paymentId = document.getElementById('payment-01').value;
        } else {
            var paymentId = document.getElementById('payment-02').value;
        }
        var paymentCurrency = "R01235";
        var manager = "moryakovl@gmail.com";
        console.log(book, name, phone, email, comment, deliveryId, deliveryAddress, paymentId, paymentCurrency);
        var body = 'book=' + encodeURIComponent(book) + '&name=' + encodeURIComponent(name) + '&phone=' + encodeURIComponent(phone) + '&email=' + encodeURIComponent(email) + '&comment=' + encodeURIComponent(comment) + '&deliveryId=' + encodeURIComponent(deliveryId) + '&deliveryAdress=' + encodeURIComponent(deliveryAddress) + '&paymentId=' + encodeURIComponent(paymentId) +'&paymentCurrency=' + encodeURIComponent(paymentCurrency) + '&manager=' + encodeURIComponent(manager);
        $http({
            url: 'https://netology-fbb-store-api.herokuapp.com/order',
            method: "POST",
            body: body,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(function(response) {
           var mainPage = document.getElementById("order-list");
           var successMessage = document.getElementById("success-message");
           var errorBlock = document.getElementById('error-block');
           mainPage.style.display = "none";
           errorBlock.style.display = "block";
           successMessage.style.display = "block";
        }, 
        function(response) {
           // console.log(response);
           var mainPage = document.getElementById("order-list");
           var errorMessage = document.getElementById("error-message");
           var err = document.getElementById("err-response-message");
           var errorBlock = document.getElementById('error-block');
           errorMessage.style.display = "block";
           errorBlock.style.display = "block";
           mainPage.style.display = "none";
           errorMessage.innerHTML = "Извините, ваш заказ не был обработан." + " Статус ответа: " + response.status + ". Ответ от сервера: " + response.data.message 
        });
    };
    $scope.showTotalPrice1 = function(){
        var bookPrice = document.getElementById('book-id').getAttribute('ng-src');
        var totalText = document.getElementById('total').innerText = "Итого к оплате: " + bookPrice + "Z";
    };
    $scope.showTotalPrice2 = function(){
        var bookPrice = document.getElementById('book-id').getAttribute('ng-src');
        var deliveryPrice = document.getElementById('delivery-02').getAttribute('ng-src');
        var totalPrice = parseInt(bookPrice) + parseInt(deliveryPrice);
        var totalText = document.getElementById('total').innerText = "Итого к оплате: " + totalPrice + "Z";
    };
    $scope.master= {};

    $scope.reset = function() {
    $scope.user = angular.copy($scope.master);
    };

    $scope.isUnchanged = function(user) {
    return angular.equals(user, $scope.master);
    };
});
console.log('hello from controller.js');
