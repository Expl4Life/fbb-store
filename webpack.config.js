'use strict';

var NODE_ENV = process.env.NODE_ENV || 'development';
var webpack = require('webpack');
module.exports = {
	entry: {
        main: ['./src/js/app', './src/js/controllers'],
        libs: ['./src/js/libs']
    },
    output: {
        path: __dirname + '/src/js-production/',
        publicPath: '/',
        filename: "[name].js"
    },
    // watch: true,
    watch: NODE_ENV == 'development',
    watchOptions: {
    	aggregateTimeout: 100
    },

    devtool: NODE_ENV == 'development' ? 'cheap-inline-module-source-map': null,

    // plugins: [
    //     new webpack.NoErrorsPlugin(),
    // 	new webpack.DefinePlugin({
    // 		NODE_ENV: JSON.stringify(NODE_ENV)
    // 	}),
    //     new webpack.optimize.CommonsChunkPlugin({
    //         name: 'common'
    //         // chunks: ['about', 'home']
    //     })
    //     new webpack.optimize.UglifyJsPlugin({
    //         compress: {
    //             warnings: false
    //         }
    //     })
    // ],

    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery/dist/jquery.min.js",
        jQuery: "jquery/dist/jquery.min.js",
        "window.jQuery": "jquery/dist/jquery.min.js"
      })
    ],

    devServer: {
        host: 'localhost',
        port: 8080,
        // contentBase: __dirname + 'src/',
        hot: true
    },
};